package co.com.prueba.tecnica.colegio.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "profesor_pro")
public class Profesor {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pro_id", nullable=false, unique=true)
	private int proId;
	@Column(name="pro_nombre", nullable=false, unique=false)
    private String proNombre;
    
    @OneToMany(
            fetch = FetchType.LAZY,
    		mappedBy = "profesor",
            cascade = CascadeType.ALL
    )
    private List<Asignatura> asignaturas;
}
