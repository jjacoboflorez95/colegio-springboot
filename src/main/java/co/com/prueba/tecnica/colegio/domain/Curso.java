package co.com.prueba.tecnica.colegio.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "curso_cur")
public class Curso {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cur_id", nullable=false, unique=true)
    private int curId;
	@Column(name="cur_grado", nullable=false, unique=false)
    private int curGrado;
	@Column(name="cur_salon", nullable=false, unique=false)
    private String curSalon;
	//Llave foránea
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_colegio")
    private Colegio colegio;
    
    @OneToMany(
            fetch = FetchType.LAZY,
    		mappedBy = "curso",
            cascade = CascadeType.ALL
    )
    private List<Asignatura> asignaturas;
}
