package co.com.prueba.tecnica.colegio.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import co.com.prueba.tecnica.colegio.domain.Curso;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AsignaturaDto implements Serializable{

	private Integer asiId;
    private String asiNombre;
    private Curso curso;
}
