package co.com.prueba.tecnica.colegio.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "asignatura_asi")
public class Asignatura {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="asi_id")
    private int asiId;
	@Column(name="asi_nombre")
    private String asiNombre;
    
    
	//Llaves foráneas
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_profesor")
    private Profesor profesor;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_curso")
    private Curso curso;
    
    @OneToMany(
            fetch = FetchType.LAZY,
    		mappedBy = "asignatura",
            cascade = CascadeType.ALL
    )
    private List<AsignaturaEstudiante> asignaturasEstudiantes;   
}
