package co.com.prueba.tecnica.colegio.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.prueba.tecnica.colegio.service.ProfesorService;
import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin
@RequestMapping(value="/profesor")
@RequiredArgsConstructor
public class ProfesorController {

	private final ProfesorService profesorService;
	
	@GetMapping
    public ResponseEntity<Object> consultarProfesores() {	
        return ResponseEntity.ok(profesorService.consultarProfesores());
    }
}
