package co.com.prueba.tecnica.colegio.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.prueba.tecnica.colegio.dto.ProfesorDto;
import co.com.prueba.tecnica.colegio.repository.ProfesorRepository;

@Service
public class ProfesorServiceImpl implements ProfesorService{

	@Autowired
	private ProfesorRepository profesorRepository;	
	
	/**
	 * Método que se encarga consultar los profesores.
	 */
	@Override
	public List<ProfesorDto> consultarProfesores(){
		List<ProfesorDto> listaProfesores = profesorRepository.findAll().stream().map(profesor -> {
			ProfesorDto profesorDto = new ProfesorDto();
			profesorDto.setProId(profesor.getProId());
			profesorDto.setProNombre(profesor.getProNombre());
			return profesorDto;
		}).collect(Collectors.toList());
		return listaProfesores;
	}
}
