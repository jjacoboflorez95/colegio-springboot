package co.com.prueba.tecnica.colegio.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import co.com.prueba.tecnica.colegio.domain.Asignatura;
import co.com.prueba.tecnica.colegio.domain.Estudiante;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AsignaturaEstudianteDto implements Serializable{

	private Integer asiestId;
	private Asignatura asignatura;
	private Estudiante estudiante;
}
