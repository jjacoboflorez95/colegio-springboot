package co.com.prueba.tecnica.colegio.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "asignaturaestudiante_asiest")
public class AsignaturaEstudiante {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="asiest_id", nullable=false, unique=true)
    private int asiestId;
    
    //Llaves foráneas
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_asignatura")
    private Asignatura asignatura;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_estudiante")
    private Estudiante estudiante;
}
