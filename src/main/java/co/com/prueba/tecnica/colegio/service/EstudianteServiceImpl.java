package co.com.prueba.tecnica.colegio.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;

import co.com.prueba.tecnica.colegio.domain.Asignatura;
import co.com.prueba.tecnica.colegio.domain.QAsignatura;
import co.com.prueba.tecnica.colegio.domain.QAsignaturaEstudiante;
import co.com.prueba.tecnica.colegio.domain.QEstudiante;
import co.com.prueba.tecnica.colegio.dto.EstudianteDto;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EstudianteServiceImpl implements EstudianteService{

	private final QEstudiante qEstudiante = QEstudiante.estudiante;
	private final QAsignatura qAsignatura = QAsignatura.asignatura;
	private final QAsignaturaEstudiante qAsignaturaEstudiante = QAsignaturaEstudiante.asignaturaEstudiante;
	
	@PersistenceContext
    private EntityManager entityManager;
	
	/**
	 * Método que se encarga consultar los estudiantes, de una asignatura en específico.
	 * Se utiliza paginación, tratando de ponerlo como un caso real donde habrán muchos datos y es necesario tenerla,
	 * para este caso, no se coloco ninguna indicación en la url del proyecto de Angular, se dejo el valor por defecto.
	 */
	@Override
	public Page<EstudianteDto> consultarEstudiantes(int idAsignatura, Pageable pageable) {
		JPAQuery<Asignatura> query = new JPAQuery<>(entityManager);
		query = query.from(qAsignaturaEstudiante);
		 query = query.innerJoin(qAsignaturaEstudiante.estudiante, qEstudiante)
				 .innerJoin(qAsignaturaEstudiante.asignatura, qAsignatura);
		 query = query.where(qAsignatura.asiId.eq(idAsignatura));
		
		List<EstudianteDto> resultSet = query
                .select(Projections.constructor(EstudianteDto.class,
                		qEstudiante.estId,
                		qEstudiante.estNombre))
                .limit(pageable.getPageSize()).offset(pageable.getOffset())
                .fetch();
		Long total = query.fetchCount();
        return new PageImpl<>(resultSet, pageable, total);
	}
}
