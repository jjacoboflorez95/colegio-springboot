package co.com.prueba.tecnica.colegio.service;

import java.util.List;

import co.com.prueba.tecnica.colegio.dto.ProfesorDto;

public interface ProfesorService {
	
	List<ProfesorDto> consultarProfesores();
}
