package co.com.prueba.tecnica.colegio.domain;

import lombok.Data;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "estudiante_est")
public class Estudiante {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="est_id", nullable=false, unique=true)
	private int estId;
	@Column(name="est_nombre", nullable=false, unique=false)
    private String estNombre;
    
    @OneToMany(
            fetch = FetchType.LAZY,
    		mappedBy = "estudiante",
            cascade = CascadeType.ALL
    )
    private List<AsignaturaEstudiante> asignaturasEstudiantes;
} 
