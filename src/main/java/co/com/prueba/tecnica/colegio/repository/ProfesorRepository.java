package co.com.prueba.tecnica.colegio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;

import co.com.prueba.tecnica.colegio.domain.Profesor;
import co.com.prueba.tecnica.colegio.domain.QProfesor;

public interface ProfesorRepository extends JpaRepository<Profesor, Integer>, QuerydslPredicateExecutor<Profesor>, QuerydslBinderCustomizer<QProfesor>{

	@Override
    default void customize(QuerydslBindings bindings, QProfesor root) {
        bindings.bind(String.class).first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
