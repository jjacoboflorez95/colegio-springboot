package co.com.prueba.tecnica.colegio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;

import co.com.prueba.tecnica.colegio.domain.Asignatura;
import co.com.prueba.tecnica.colegio.domain.QAsignatura;

public interface AsignaturaRepository extends JpaRepository<Asignatura, Integer>, QuerydslPredicateExecutor<Asignatura>, QuerydslBinderCustomizer<QAsignatura>{
	
	@Override
    default void customize(QuerydslBindings bindings, QAsignatura root) {
        bindings.bind(String.class).first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
