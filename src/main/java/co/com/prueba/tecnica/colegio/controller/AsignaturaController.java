package co.com.prueba.tecnica.colegio.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.prueba.tecnica.colegio.dto.AsignaturaCursoDto;
import co.com.prueba.tecnica.colegio.service.AsignaturaService;
import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin
@RequestMapping(value="/asignatura")
@RequiredArgsConstructor
public class AsignaturaController {
	
	private final AsignaturaService asignaturaService;
	
	@GetMapping("/idprofesor/{idProfesor}")
    public ResponseEntity<Page<AsignaturaCursoDto>> consultarAsignaturas(@PathVariable int idProfesor, Pageable pageable) {	
        return ResponseEntity.ok(asignaturaService.consultarAsignaturas(idProfesor, pageable));
    }	
}
