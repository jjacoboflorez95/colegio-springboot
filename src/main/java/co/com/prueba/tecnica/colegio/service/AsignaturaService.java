package co.com.prueba.tecnica.colegio.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.prueba.tecnica.colegio.dto.AsignaturaCursoDto;

public interface AsignaturaService {
	
	Page<AsignaturaCursoDto> consultarAsignaturas(int idProfesor, Pageable pageable);
}
