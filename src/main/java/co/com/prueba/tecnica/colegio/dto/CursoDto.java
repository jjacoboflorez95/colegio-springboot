package co.com.prueba.tecnica.colegio.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import co.com.prueba.tecnica.colegio.domain.Colegio;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CursoDto implements Serializable{

	private Integer curId;
	private Integer curGrado;
	private String curSalon;
	private Colegio colegio;
}
