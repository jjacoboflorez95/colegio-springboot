package co.com.prueba.tecnica.colegio.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.prueba.tecnica.colegio.dto.EstudianteDto;
import co.com.prueba.tecnica.colegio.service.EstudianteService;
import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin
@RequestMapping(value="/estudiante")
@RequiredArgsConstructor
public class EstudianteController {

	private final EstudianteService estudianteService;
	
	@GetMapping("/idasignatura/{idAsignatura}")
    public ResponseEntity<Page<EstudianteDto>> consultarAsignaturas(@PathVariable int idAsignatura, Pageable pageable) {	
        return ResponseEntity.ok(estudianteService.consultarEstudiantes(idAsignatura, pageable));
    }
}
