package co.com.prueba.tecnica.colegio.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;

import co.com.prueba.tecnica.colegio.domain.Asignatura;
import co.com.prueba.tecnica.colegio.domain.QAsignatura;
import co.com.prueba.tecnica.colegio.domain.QCurso;
import co.com.prueba.tecnica.colegio.domain.QProfesor;
import co.com.prueba.tecnica.colegio.dto.AsignaturaCursoDto;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AsignaturaServiceImpl implements AsignaturaService{
	
	/*@Autowired
	private AsignaturaRepository asignaturaRepository;*/
	private final QAsignatura qAsignatura = QAsignatura.asignatura;
	private final QProfesor qProfesor = QProfesor.profesor;
	private final QCurso qCurso = QCurso.curso;
	
	@PersistenceContext
    private EntityManager entityManager;		
	
	/**
	 * Método que se encarga consultar las asignaturas, junto con su curso, de un profesor en específico.
	 * Se utiliza paginación, tratando de ponerlo como un caso real donde habrán muchos datos y es necesario tenerla,
	 * para este caso, no se coloco ninguna indicación en la url del proyecto de Angular, se dejo el valor por defecto.
	 */
	@Override
    public Page<AsignaturaCursoDto> consultarAsignaturas(int idProfesor, Pageable pageable) {
		JPAQuery<Asignatura> query = new JPAQuery<>(entityManager);
		query = query.from(qAsignatura);
		 query = query.innerJoin(qAsignatura.curso, qCurso)
				 .innerJoin(qAsignatura.profesor, qProfesor);
		 query = query.where(qProfesor.proId.eq(idProfesor));
		
		List<AsignaturaCursoDto> resultSet = query
                .select(Projections.constructor(AsignaturaCursoDto.class,
                		qAsignatura.asiId,
                		qAsignatura.asiNombre,
                		qCurso.curGrado.stringValue().concat(qCurso.curSalon)))
                .limit(pageable.getPageSize()).offset(pageable.getOffset())
                .fetch();
		Long total = query.fetchCount();
        return new PageImpl<>(resultSet, pageable, total);
	}	
}
