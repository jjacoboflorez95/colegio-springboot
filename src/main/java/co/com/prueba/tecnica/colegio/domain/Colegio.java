package co.com.prueba.tecnica.colegio.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "colegio_col")
public class Colegio {
	
	@Id  
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="col_id", nullable=false, unique=true)
    private int colId;
	@Column(name="col_nombre", nullable=false, unique=false)
    private String colNombre;
	
    @OneToMany(
            fetch = FetchType.LAZY,
    		mappedBy = "colegio",
            cascade = CascadeType.ALL
    )
    private List<Curso> cursos;    
    
}
