package co.com.prueba.tecnica.colegio.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.prueba.tecnica.colegio.dto.EstudianteDto;

public interface EstudianteService {

	Page<EstudianteDto> consultarEstudiantes(int idAsignatura, Pageable pageable);
}
