INSERT INTO COLEGIO_COL (COL_ID, COL_NOMBRE) VALUES (1, 'Coliseo Francés');

INSERT INTO CURSO_CUR (CUR_ID, CUR_GRADO, CUR_SALON, ID_COLEGIO) VALUES 
(1,10,'A',1),
(2,10,'B',1),
(3,11,'A',1),
(4,11,'B',1);

INSERT INTO PROFESOR_PRO (PRO_ID, PRO_NOMBRE) VALUES 
(1,'Némesis'),
(2,'Príapo'),
(3,'Iris');

INSERT INTO ASIGNATURA_ASI (ASI_ID , ASI_NOMBRE, ID_CURSO, ID_PROFESOR) VALUES 
(1,'Matemáticas', 1,1),
(2,'Matemáticas', 2,1),
(3,'Matemáticas', 3,1),
(4,'Matemáticas', 4,1),
(5,'Español', 1,2),
(6,'Español', 2,2),
(7,'Inglés Básico', 1,3),
(8,'Inglés Avanzado', 2,3),
(9,'Pre Icfes', 3,1),
(10,'Pre Icfes', 4,1);

INSERT INTO ESTUDIANTE_EST (EST_ID, EST_NOMBRE ) VALUES 
(1,'Afrodita'),
(2,'Apolo'),
(3,'Ares'),
(4,'Artemisa'),
(5,'Atenea'),
(6,'Dionisio'),
(7,'Hefesto'),
(8,'Hera'),
(9,'Hermes'),
(10,'Hades'),
(11,'Poseidón'),
(12,'Zeus');

INSERT INTO ASIGNATURAESTUDIANTE_ASIEST (ASIEST_ID, ID_ASIGNATURA, ID_ESTUDIANTE) VALUES 
(1,1,1),
(2,5,1),
(3,7,1),
(4,1,2),
(5,5,2),
(6,7,2),
(7,1,3),
(8,5,3),
(9,7,3),
(10,2,4),
(11,6,4),
(12,8,4),
(13,2,5),
(14,6,5),
(15,8,5),
(16,2,6),
(17,6,6),
(18,8,6),
(19,3,7),
(20,9,7),
(21,3,8),
(22,9,8),
(23,4,9),
(24,10,9),
(25,4,10),
(26,10,10),
(27,4,11),
(28,10,11),
(29,4,12),
(30,10,12);